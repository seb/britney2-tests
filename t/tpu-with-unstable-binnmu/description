Test for binNMU in unstable with unblock for t-p-u

This test tests the combination of a binNMU in unstable and a new source in
testing-proposed-updates.

When both the binNMU and the source are candidates, the result depends on the
order in which they are tried. If the binNMU is tried after the source is
accepted, the binaries from the binNMU will be accepted, even though the
source in testing is no longer the same. The binaries from the binNMU will
then be removed as britney considers them old libs. The result is that the
binaries on the binMNU arch are missing.

This test has a force-hint for the source in t-p-u, to make sure that is
processed first. The force hint doesn't actually force anything, it just
changes the order.

There is a variant without a force-hint. In this case the order is undefined
and the result depends on the (random) order. This variant might pass or fail,
depending on this order, which might change due to unrelated changes.
Currently (2018-12-17) the dependency for the binNMU on a binary from another
source makes britney try the binNMU after the source from t-p-u.

This test is based on binnmu-tpu, with the binNMU and the new source reversed.

Please note that the situation of this test should not happen: having a higher
version in t-p-u or testing than in unstable is something that normally
doesn't happen. A stable update or security update in stable-proposed-updates
could have a higher version, but it shouldn't be accepted if unstable has a
lower version.

