#! /usr/bin/perl

use strict;

use DBI;
#use YAML::Syck;
use JSON;

use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

my $binaries = {};

my $bins = {};

$bins->{"maintaineruploada"} = {amd64 => 0, i386 => 1};
$bins->{"maintaineruploadb"} = {amd64 => 0, i386 => 1};
$bins->{"maintainerupload-common"} = {all => 0};

$bins->{"maintaineruploadall"} = {amd64 => 1, i386 => 1};
$bins->{"maintaineruploadall-common"} = {all => 0};

$bins->{"maintaineruploadarch"} = {amd64 => 0, i386 => 1};
$bins->{"maintaineruploadarch-common"} = {all => 1};

$bins->{"maintainerupload2"} = {amd64 => 0, i386 => 1};

$bins->{"sourceuploada"} = {amd64 => 1, i386 => 1};
$bins->{"sourceuploadb"} = {amd64 => 1, i386 => 1};
$bins->{"sourceupload-common"} = {all => 1};

$bins->{"whitelisted1"} = {amd64 => 0, i386 => 1};
$bins->{"whitelisted1-common"} = {all => 0};
$bins->{"whitelisted1-doc"} = {all => 0};

$bins->{"whitelisted2"} = {amd64 => 1, i386 => 1};
$bins->{"whitelisted2-common"} = {all => 0};

$bins->{"bin-contrib"} = {amd64 => 0};

$bins->{"maintainerbinnmua"} = {amd64 => {version => "1.0-1+b1",buildd=>0}, i386 => 1};
$bins->{"maintainerbinnmub"} = {amd64 => {version => "1.0-1+b1",buildd=>0}, i386 => 1};
$bins->{"maintainerbinnmu-common"} = {all => {version => "1.0-1",buildd=>1}};

$bins->{"builddbinnmua"} = {amd64 => {version => "1.0-1+b1",buildd=>1}, i386 => 1};
$bins->{"builddbinnmub"} = {amd64 => {version => "1.0-1+b1",buildd=>1}, i386 => 1};
$bins->{"builddbinnmu-common"} = {all => {version => "1.0-1",buildd=>1}};

$bins->{"mixedbinnmua"} = {amd64 => {version => "1.0-1+b1",buildd=>1}, i386 => 1};
$bins->{"mixedbinnmub"} = {amd64 => {version => "1.0-1+b1",buildd=>1}, i386 => 1};
$bins->{"mixedbinnmu-common"} = {all => {version => "1.0-1",buildd=>0}};

foreach my $bin (keys %$bins) {
	foreach my $arch (keys %{$bins->{$bin}}) {
		my $version = "1.0-2";
		my $info = $bins->{$bin}->{$arch};
		if (ref $info eq "HASH") {
			$version = $info->{"version"};
			$info = $info->{"buildd"};
		}
		if ($info) {
			$binaries->{$bin}->{$version}->{$arch}->{"uid"} = "buildd_amd64\@buildd.debian.org";
			$binaries->{$bin}->{$version}->{$arch}->{"buildd"} = JSON::true;
		} else {
			$binaries->{$bin}->{$version}->{$arch}->{"uid"} = "uploader";
			$binaries->{$bin}->{$version}->{$arch}->{"buildd"} = JSON::false;
		}
	}
}

print to_json($binaries,{pretty => 1, canonical => 1});

