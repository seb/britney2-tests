# perl

# Copyright 2011 Niels Thykier <niels@thykier.net>
# License GPL-2 or (at your option) any later.

package BritneyTest;

use strict;
use warnings;
use autodie;

use parent qw(Class::Accessor);
use Exporter qw(import);

use constant {
    SUCCESS_EXPECTED => 0,
    FAILURE_UNEXPECTED => 1,
    SUCCESS_UNEXPECTED => 2,
    FAILURE_EXPECTED => 3,
    ERROR_EXPECTED => 4,
};

use Carp qw(croak);
use Dpkg::Control;

use File::Find;

use Expectation;
use SystemUtil;
use TestLib;
use CompareExcuses;
use CompareAutopkgtest;

our @EXPORT = qw(
      SUCCESS_EXPECTED
      FAILURE_UNEXPECTED
      SUCCESS_UNEXPECTED
      FAILURE_EXPECTED
      ERROR_EXPECTED
);

my $DEFAULT_ARCH = 'i386';
my @AUTO_CREATE_EMPTY = (
    'state/rc-bugs-testing',
    'state/rc-bugs-unstable',
);
my %SUITE_DIRNAME2CONFIG_NAME = (
    'unstable' => 'UNSTABLE',
    'testing' => 'TESTING',
    'testing-proposed-updates' => 'TPU',
    'proposed-updates' => 'PU',
);

sub new {
    my ($class, $testdata, $rundir, $testdir, $variant) = @_;
    my $self = {
        'rundir'   => $rundir,
        'testdir'  => $testdir,
        'testdata' => $testdata,
        'variant'  => $variant,
    };
    bless $self, $class;
    return $self;
}

sub _merge_dir {
    my ($self, $subdir) = @_;
    my $testdir = $self->testdir;
    my $rundir = $self->rundir;
    my $variant = $self->variant;
    my $target_dir = "$rundir/$subdir";
    $subdir .= '/' if $subdir and $subdir !~ m{/$};
    $target_dir = "$rundir/$subdir";
    if (not -d $target_dir) {
        system('mkdir', '-p', $target_dir) == 0 or croak("mkdir -p $target_dir failed");
    }
    if (-d "$testdir/$subdir") {
        system('rsync', '-a', "$testdir/$subdir", $target_dir) == 0 or
            croak("rsync failed: " . (($?>>8) & 0xff));
    }
    if ($variant and -d "${testdir}/variants/$variant/$subdir") {
        system('rsync', '-aI', "${testdir}/variants/${variant}/${subdir}", $target_dir) == 0 or
            croak("rsync for variant failed: " . (($?>>8) & 0xff));
    }
    return $target_dir;
}

sub _maybe_merge_dir {
    my ($self, $subdir, $create_if_missing) = @_;
    my $testdir = $self->testdir;
    my $variant = $self->variant;
    my @choices = ("${testdir}/${subdir}");
    push(@choices, "${testdir}/variants/${variant}/${subdir}") if $variant;
    @choices = grep { -d } @choices;

    # Does not exist;
    if (not @choices) {
        if ($create_if_missing) {
            my $rundir = $self->rundir;
            my $target_dir = "${rundir}/${subdir}";
            system('mkdir', '-p', $target_dir) == 0
                or croak("mkdir -p ${target_dir} failed");
            return $target_dir;
        }
        return;
    }

    # Exactly one match
    return $choices[0] if scalar(@choices) == 1;

    # More than one match, merge for now.
    goto \&_merge_dir;
}

sub setup {
    my ($self) = @_;
    my $rundir = $self->rundir;
    my $state_dir;
    my $input_dir;
    my (@suite_config, $outputdir);
    mkdir $rundir, 0777 or croak "mkdir $rundir: $!";

    $outputdir = "$rundir/output";
    mkdir($outputdir, 0777) or croak("mkdir $rundir: $!");

    $self->_read_test_data;

    $state_dir = $self->_merge_dir('state');
    $input_dir = $self->_maybe_merge_dir('input', 1);

    foreach my $autogen (@AUTO_CREATE_EMPTY) {
        my $f = "$rundir/$autogen";
        my $dir;

        next if -e $f;
        $dir = $f;
        $dir =~ s,/[^/]++$,,;
        unless ( -d $dir ) {
            system('mkdir', '-p', $dir) == 0 or croak("mkdir -p $dir failed");
        }
        if ($f !~ m,/$,) {
            open(my $fd, '>', $f);
            close($fd);
        }
    }

    my $hints = [];
    my $hintsdir = $self->_maybe_merge_dir('hints', 1);
    my $have_release_file = 0;
    {
        my $unstable_path;
        if (-d $hintsdir) {
            opendir(my $dd, $hintsdir);
            push @$hints, grep { !/^\./o } sort readdir($dd);
            closedir($dd);
        }
        for my $suite_dirname (sort(keys(%SUITE_DIRNAME2CONFIG_NAME))) {
            my $path = $self->_maybe_merge_dir("var/data/$suite_dirname");
            next if not defined($path);
            # _generate_urgency_dates need to know the path to unstable
            $unstable_path = $path if $suite_dirname eq 'unstable';
            $have_release_file = 1 if $suite_dirname eq 'testing' and -e "$path/Release";
            my $config_name = $SUITE_DIRNAME2CONFIG_NAME{$suite_dirname};
            push(@suite_config, "$config_name = $path");
        }

        if (not -f "${state_dir}/age-policy-urgencies" and not -f "${state_dir}/age-policy-dates") {
            croak("There is unstable!?") if not defined($unstable_path);
            $self->_generate_urgency_dates($state_dir, $unstable_path);
        }
        if (not -f "${state_dir}/piuparts-summary-unstable.json" and not -f "${state_dir}/piuparts-summary-testing.json") {
            open(my $ufd, '>', "${state_dir}/piuparts-summary-unstable.json");
            close($ufd);
            open(my $tfd, '>', "${state_dir}/piuparts-summary-testing.json");
            close($tfd);
        }
    }


    $self->_gen_britney_conf ("$rundir/britney.conf", $self->{'testdata'},
                              \@suite_config, $outputdir, $input_dir,
                              $state_dir, $hintsdir, $hints, $have_release_file,
        );

}

sub _find_test_file {
    my ($self, $filename) = @_;
    my $testdir = $self->testdir;
    my $variant = $self->variant;
    my @possible_locations = ("${testdir}/${filename}");
    if ($variant) {
        unshift(@possible_locations, "${testdir}/variants/${variant}/${filename}");
    }
    for my $file (@possible_locations) {
        return $file if -f $file;
    }
    return;
}

sub run {
    my ($self, $britney, $impl) = @_;
    my $cmd = $self->_britney_cmdline ($britney);
    my $rundir = $self->rundir;
    my $testdata = $self->{'testdata'};
    my $exp = Expectation->new;
    my $result = 0;
    my $res = Expectation->new;
    my $info = [];
    my $s = system_file("$rundir/log.txt", $cmd);
    my $heidi = "$rundir/output/HeidiResult";
    my $expected_heidi_fallback = $self->_find_test_file('expected.fallback');
    if ($s) {
        if ($impl && exists($self->{'failures'}{lc $impl})) {
            my $ex = $self->{'failures'}{lc $impl};
            if ($ex eq 'crash') {
                return ERROR_EXPECTED;
            }
            croak "$britney died with  ".(($? >> 8) & 0xff);
        }
    }
    if (my $expected = $self->_find_test_file('expected')) {
        $exp->read_heidi_file($expected);
    }

    if (!-f $heidi) {
        croak("$britney did not produce a HeidiResult at ${heidi} - perhaps a silent failure!?");
    }
    $res->read_heidi_file($heidi);

    my ($as, $rs, $ab, $rb) = $exp->diff ( $res );
    # Always create the diff (even if it would be empty)
    open(my $fd, '>', "$rundir/diff");
    my $exp_diff = 0;
    my $fb_diff = 0;

    if (($testdata->{'ignore-expected'} // '') eq 'yes') {
        # "Any" result is "ok"
        $result = 1;
    } else {
        if (@$as + @$rs + @$ab + @$rb) {
            # Failed
            $exp_diff = 1;
            Expectation::print_diff ($fd, $as, $rs, $ab, $rb);
            $result = FAILURE_UNEXPECTED;
            push @$info, "result different";
        } else {
            $result = SUCCESS_EXPECTED;
            push @$info, "result ok";
        }
    }

    close($fd);

    # if we know the test currently doesn't give the result we want, we still
    # want to check that the result doesn't change unexpectedly
    if ($exp_diff && defined($expected_heidi_fallback)) {
        my $exp_fb = Expectation->new;
        $exp_fb->read_heidi_file($expected_heidi_fallback);
        my ($as, $rs, $ab, $rb) = $exp_fb->diff ( $res );
        if (@$as + @$rs + @$ab + @$rb) {
            $fb_diff = 1;
            open(my $fd, '>', "$rundir/diff.fallback");
            Expectation::print_diff ($fd, $as, $rs, $ab, $rb);
            close($fd);
            push @$info, "fallback different";
        } else {
            push @$info, "fallback ok";
        }
    }

    if (my $expected = $self->_find_test_file('expected-excuses.yaml')) {
        my $exc_ok = compare_excuses(
                    $expected,
                    "$rundir/output/excuses.yaml",
                    );
        unless($exc_ok) {
            my $fn = "$rundir/excuses.diff";
            open(my $fd, '>', $fn);
            my $issues = get_excuse_issues();
            print $fd join("\n",@$issues)."\n";
            close($fd);

            push @$info, "excuses different";
            $result = FAILURE_UNEXPECTED;
        } else {
            push @$info, "excuses ok";
        }
    }

    if (my $expected = $self->_find_test_file('expected-debci')) {
        my $ci_ok = compare_debci(
                    $expected,
                    "$rundir/output/debci.input",
                    );
        unless($ci_ok) {
            my $fn = "$rundir/debci.diff";
            open(my $fd, '>', $fn);
            my $issues = get_debci_issues();
            print $fd join("\n",@$issues)."\n";
            close($fd);

            push @$info, "debci different";
            $result = FAILURE_UNEXPECTED;
        } else {
            push @$info, "debci ok";
        }
    }

    if ($impl and exists $self->{'failures'}{lc $impl}) {
        my $ex = $self->{'failures'}{lc $impl};
        # The implementation is expected to fail or crash, so any
        # success is "unexpected"
        $result = SUCCESS_UNEXPECTED if $result == SUCCESS_EXPECTED;
        if ($result == FAILURE_UNEXPECTED && $ex ne 'crash') {
            # We were expected to fail here.
            $result = FAILURE_EXPECTED unless ($fb_diff);
        }
    }
    return ($result, join(", ",@$info));
}

sub clean {
    my ($self) = @_;
    my $rundir = $self->rundir;
    system 'rm', '-r', $rundir == 0 or croak "rm -r $rundir failed: $?";
}

sub testdata {
    my ($self, $key) = @_;
    return $self->{'testdata'}->{$key};
}

sub _britney_cmdline {
    my ($self, $britney) = @_;
    my $rundir = $self->rundir;
    my $conf = "$rundir/britney.conf";

    return [$britney, '-c', $conf, '-v'];
}


BritneyTest->mk_ro_accessors (qw(rundir testdir variant));

sub _read_test_data {
    my ($self) = @_;
    my $dataf = $self->_find_test_file('test-data');
    my ($ctrl, @para);
    return if not defined($dataf);
    open(my $fd, '<', $dataf);
    while ( defined ($ctrl = Dpkg::Control->new (type => CTRL_UNKNOWN)) and
            ($ctrl->parse ($fd, $dataf)) ) {
        push(@para, $ctrl);
    }
    close($fd);
    $self->{'testdata'} = shift @para;
    if ($self->{'testdata'}->{'expected-failure'}) {
        my $rawfield = $self->{'testdata'}{'expected-failure'};
        my %impl;
        for my $entry (grep { $_ } split(m/(?:\s++|\n)++/o, $rawfield)) {
            my $ex = 'failure';
            if (index($entry, '=') > -1) {
                ($entry, $ex) = split('=', $entry, 2);
            }
            $impl{lc $entry} = $ex;
        }
        $self->{'failures'} = \%impl;
    }
}

sub _generate_urgency_dates {
    my ($self, $state_dir, $siddir) = @_;
    my $urgen = "${state_dir}/Test-urgency.in";
    my $dates = {};
    my $urgencies = {};
    my @sources = ();
    my $ctrl;

    if (! -d $siddir) {
        croak "$siddir is not a directory";
    }

    my @allsidsources = ();
    find( sub { push @allsidsources, $File::Find::name if "$_" eq "Sources"; },
          $siddir );

    for my $sidsources (@allsidsources) {
        open(my $fd, '<', $sidsources);
        while ( defined ($ctrl = Dpkg::Control->new (type => CTRL_INDEX_SRC)) and
                ($ctrl->parse ($fd, $sidsources)) ) {
            my $source = $ctrl->{'Package'};
            my $version = $ctrl->{'Version'};
            croak "$sidsources contains a bad entry!"
                unless defined $source and defined $version;
            push @sources, [$source, $version];
            $urgencies->{"$source/$version"} = 'low';
            $dates->{"$source/$version"} = 1;
        }
        close $fd;
    }

    if ( -f $urgen ) {
        # Load the urgency generation hints.
        # Britney's day begins at 7pm. (see britney2/policies/policy.py)
        my $bnow = int (((time / (60 * 60)) - 19) / 24);
        open(my $fd, '<', $urgen);
        while ( my $line = <$fd> ) {
            chomp $line;
            next if $line =~ m/^\s*(?:\#|\z)/o;
            my ($srcver, $date, $urgency) = split m/\s++/, $line, 3;
            croak "Cannot parse line $. in $urgen."
                unless defined $srcver and defined $date and
                       defined $urgency;
            croak "Reference to unknown source $srcver ($urgen: $.)"
                unless exists $urgencies->{$srcver};
            croak "Unknown urgency for $srcver ($urgen: $.)"
                unless $urgency =~ m/^(low|medium|high|emergency|critical)$/;
            if ($date eq '*') {
                $date = 1;
            } elsif ($date =~ m/^age=(\d+)$/o) {
                $date = $bnow - $1;
            } elsif ($date !~ m/^\d+$/o) {
                croak "Date for $srcver is not an int ($urgen: $.)";
            }
            $urgencies->{$srcver} = $urgency;
            $dates->{$srcver} = $date;
        }
        close $fd;
    }

    TestLib::gen_dates_urgencies($state_dir, \@sources, $dates, $urgencies);
}

sub _gen_britney_conf {
    my ($self, $file, $data, $suite_config, $outputdir, $inputdir, $state_dir, $hintdir, $hints, $have_release_file) = @_;

    my $archs   = "# set in Release file";
    unless ($have_release_file) {
        $archs = "ARCHITECTURES     = ".($data->{'architectures'}//$DEFAULT_ARCH);
    }
    my $nbarchs = $data->{'no-break-architectures'}//$DEFAULT_ARCH;
    my $farchs  = $data->{'outofsync-architectures'}//$data->{'fucked-architectures'}//'';
    my $barchs  = $data->{'break-architectures'}//'';
    my $abarch  = $data->{'all-buildarch'}//'';
    my $igcruft = $data->{'ignore-cruft'}//'1';
    my $adten   = $data->{'adt-enable'}//'yes';
    my $adtarch = $data->{'adt-arches'}//'amd64';
    my $penalty = $data->{'adt-regression-penalty'}//'10';
    my $bounty  = $data->{'adt-success-bounty'}//'3';
    my $baseline= $data->{'adt-baseline'}//'reference';
    my $defurg  = $data->{'default-urgency'}//'low';
    my $buildd  = $data->{'check-buildd'}//'no';
    my $smooth  = $data->{'smooth-updates'}//'libs oldlibs';

    my $fakeruntime = "# not set";
    if (defined($data->{'fake-runtime'})) {
        $fakeruntime = "FAKE_RUNTIME = ".$data->{'fake-runtime'};
    }

    my $concheckl = $data->{"check-consistency-level"}//3;

    my $hintperms = "";
    foreach my $hint (@$hints) {
        $hintperms .= "HINTS_".(uc $hint)."  = ALL\n";
    }

    open(my $fd, '>', $file);
    # contents of the conf
    print {$fd} <<EOF;
# Configuration file for britney

# Paths for control files
EOF
    for my $config_line (@{$suite_config}) {
        print {$fd} "$config_line\n";
    }
    print {$fd} <<EOF;

# Output
NONINST_STATUS      = $outputdir/non-installable-status
EXCUSES_OUTPUT      = $outputdir/excuses.html
UPGRADE_OUTPUT      = $outputdir/output.txt
HEIDI_OUTPUT        = $outputdir/HeidiResult
EXCUSES_YAML_OUTPUT = $outputdir/excuses.yaml

STATIC_INPUT_DIR   = $inputdir
STATE_DIR          = $state_dir

# List of release architectures
$archs

# if you're not in this list, arch: all packages are allowed to break on you
NOBREAKALL_ARCHES = $nbarchs

ALL_BUILDARCH = $abarch

# if you're in this list, your packages may not stay in sync with the source
FUCKED_ARCHES     = $farchs
OUTOFSYNC_ARCHES  = $farchs

# if you're in this list, your uninstallability count may increase
BREAK_ARCHES      = $barchs

# if you're in this list, you are a new architecture
NEW_ARCHES        =

# fake runtime for calculation delays
$fakeruntime

# check consistency
CHECK_CONSISTENCY_LEVEL = $concheckl

# priorities and delays
MINDAYS_LOW       = 10
MINDAYS_MEDIUM    = 5
MINDAYS_HIGH      = 2
MINDAYS_CRITICAL  = 0
MINDAYS_EMERGENCY = 0
DEFAULT_URGENCY   = $defurg
NO_PENALTIES      = high critical emergency

# hint permissions
HINTSDIR = $hintdir
$hintperms

# support for old libraries in testing (smooth update)
# use ALL to enable smooth updates for all the sections
SMOOTH_UPDATES = $smooth

IGNORE_CRUFT   = $igcruft

CHECK_BUILDD      = $buildd

ADT_ENABLE        = $adten
ADT_ARCHES        = $adtarch
ADT_AMQP          = file://$outputdir/debci.input
ADT_PPAS          =
ADT_SHARED_RESULTS_CACHE =
ADT_SWIFT_URL     = file://$state_dir/debci.json
ADT_CI_URL        = https://ci.debian.net/

# Autopkgtest results can be used to influence the aging
ADT_REGRESSION_PENALTY = $penalty
ADT_SUCCESS_BOUNTY     = $bounty
ADT_BASELINE           = $baseline
ADT_RETRY_OLDER_THAN   = 100000

EOF
    close($fd);
}

1;

