# perl

# Copyright 2011 Niels Thykier <niels@thykier.net>
# License GPL-2 or (at your option) any later.

package SystemUtil;

use strict;
use warnings;
use autodie;

use Exporter qw(import);

use POSIX qw(nice);

use constant MIN_OOM_ADJ => 500;

our @EXPORT = (qw(system_file open_gz open_xz));

BEGIN {
    eval { require PerlIO::gzip };
    if ($@) {
        *open_gz = \&__open_gz_ext;
    } else {
        *open_gz = \&__open_gz_pio;
    }
}

sub system_file {
    my ($file, $cmd) = @_;
    my $fd;
    my $pid = fork;
    $ENV{PERLLIB} = join ':',@INC;
    $ENV{PYTHONHASHSEED} = 'random' if not exists($ENV{PYTHONHASHSEED});

    if ($ENV{CI} and not $pid) {
        # On travis, pipe output through tee, so travis can see that we are still alive!
        my $sub_pid = open($fd, '|-');
        if (not $sub_pid) {
            require POSIX;
            my $exit = 0;
            eval {
                my $length = 0;
                open(my $log_fd, '>', $file);
                # Enable when debugging; doesn't make sense on travis
                # $log_fd->autoflush;
                while (my $read_line = <STDIN>) {
                    print {$log_fd} $read_line;
                    if ($length > 800) {
                        print "\n";
                        $length = 0;
                    }
                    print '.' if ($length % 10) == 0;
                    $length++;
                }
                close(STDIN);
                close($log_fd) or die("close $file: $!");
                print "\n";
            };
            if (my $err = $@) {
                print STDERR $err;
                $exit = 255;
            }
            POSIX::_exit($exit);
        }
    } else {
        open($fd, '>', $file);
    }

    $fd->autoflush;

    unless ($pid) {
        # child - [Linux] Ensure that the OOM killer considers
        # us a target in low memory conditions.
        if ( -e '/proc/self/oom_score_adj') {
            open(my $oom_fd, '<', '/proc/self/oom_score_adj');
            my $score = <$oom_fd>;
            chomp($score);
            close($oom_fd);
            if ($score < MIN_OOM_ADJ) {
                # Re-open oom_adj (it doesn't like seeking)
                open($oom_fd,  '>', '/proc/self/oom_score_adj');
                print {$oom_fd} MIN_OOM_ADJ . "\n";
                close($oom_fd);
            }

        }
        # Try to apply nice-ness, but if it fails then ignore it.
        nice(10) or 1;
        # re-direct STDERR to STDOUT and exec
        open(STDOUT, '>&', $fd);
        open(STDERR, '>&', \*STDOUT);
        STDOUT->autoflush;
        STDERR->autoflush;
        exec @$cmd or do {
            require POSIX;
            print STDERR "exec @$cmd failed: $!\n";
            close(STDERR);
            close(STDOUT);
            POSIX::_exit(255);
        }
    }
    waitpid($pid, 0) == $pid or die("waitpid($pid, 0) failed: $!");
    my $res = $?;
    if ($ENV{CI} and $?) {
        system('tail', '-n100', $file);
    }
    return $res;
}

# Preferred implementation of open_gz (used if the perlio layer
# is available)
sub __open_gz_pio {
    my ($file) = @_;
    open(my $fd, '<:gzip', $file);
    return $fd;
}

# Fallback implementation of open_gz
sub __open_gz_ext {
    my ($file) = @_;
    open(my $fd, '-|', 'gzip', '-dc', $file);
    return $fd;
}

sub open_xz {
    my ($file) = @_;
    open(my $fd, '-|', 'xz', '-dc', $file);
    return $fd;
}
