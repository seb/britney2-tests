# perl

# Copyright 2011 Niels Thykier <niels@thykier.net>
# License GPL-2 or (at your option) any later.

package TestLib;

use strict;
use warnings;
use autodie;

# gen_dates_urgencies ($state_dir, $spkgs, $dates, $urgencies)
#
# $tdir is the path to the state dir (where age-policy* files are to be placed)
# $spkgs is a list ref of ['package', 'version'] entries
#  - i.e. [['lintian', '2.5.4'], ['eclipse', '3.7.1']]
# $dates is a hashref mapping "$pkg/$ver" to a date
#  - if an entry is not present, it is assumed to be "1"
#  - if $dates is undefined, all dates are assumed to be "1"
# $urgencies is a hashref mapping "$pkg/$ver" to an urgency
#  - if an entry is not present, it is assumed to be "low"
#  - if $urgencies is undefined, all urgencies are assumed to be "low"
sub gen_dates_urgencies {
    my ($state_dir, $spkgs, $dates, $urgencies) = @_;
    $dates = {} unless $dates;
    $urgencies = {} unless $urgencies;

    open(my $df, '>', "${state_dir}/age-policy-dates");
    open(my $uf, '>', "${state_dir}/age-policy-urgencies");

    foreach my $s (@$spkgs) {
        my ($pkg,$ver) = @$s;
        my $date = $dates->{"$pkg/$ver"}//1;
        my $urgen = $urgencies->{"$pkg/$ver"}//'low';
        print $df "$pkg $ver $date\n";
        print $uf "$pkg $ver $urgen\n";
    }

    close($df);
    close($uf);
}

sub find_tests {
    my ($testset, $testname) = @_;
    my @tests;
    if (defined($testname) and substr($testname, 0, 1) ne '/') {
        my $t = $testname;
        $t =~ s;/.*;;;
        if (not -d "$testset/$t") {
            die "No test named $t - did you mean /$t/\n";
        }
        @tests = ($testname);
    } else {
        my $match = qr/./;
        if (defined($testname)) {
            if (    substr($testname, 0, 1) eq '/'
                    and substr($testname, -1, 1) eq '/') {
                my $regex = substr($testname, 1, length($testname) -2);
                $match = qr/$regex/;
            } else {
                die("The filter regex must start and end with \"/\"\n");
            }
        }
        opendir(my $dd, $testset);
        @tests = grep { !/^\./o and /$match/ } sort readdir($dd);
        closedir($dd);
    }
    return @tests;
}

1;

