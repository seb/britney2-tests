#!/usr/bin/perl

# Copyright 2011 Niels Thykier <niels@thykier.net>
# License GPL-2 or (at your option) any later.

use strict;
use warnings;

BEGIN {
    my $dir = $ENV{'TEST_ROOT'}||'.';
    $ENV{'TEST_ROOT'} = $dir;
}

use lib "$ENV{'TEST_ROOT'}/perl-lib";

use BritneyTest;
use TestLib;
use Getopt::Long;

my %opt = (
    'keep-going'  => 0,
);
my %opthash = (
    'keep-going|k!' => \$opt{'keep-going'},
    'help|h'        => \&_usage,
);

my $prog = $0;
$prog =~ s,[^/]*/,,g;
$prog =~ s/\.pl$//;

# init commandline parser
Getopt::Long::config ('bundling', 'no_getopt_compat', 'no_auto_abbrev');
STDOUT->autoflush;

# process commandline options
GetOptions (%opthash) or die "error parsing options, run with --help for more info\n";

my $create_test = sub { return BritneyTest->new (@_); };
my $impl = 'britney2';

my ($britney, $TESTSET, $RUNDIR, $testname) = @ARGV;

my @tests;
my $failed = 0;
my $expected = 0;
my $unexpected = 0;
my $errors = 0;
my $count = 0;
my @failed = ();

die "Usage: $prog <britney> <testset> <rundir> [<testname>|</regex/>]\n"
    unless $britney && $TESTSET && $RUNDIR;
die "Testset \"$TESTSET\" does not exists\n"
    unless -d $TESTSET;

my ($ts, $tf) = _load_timer();

mkdir $RUNDIR, 0777 or die "mkdir $RUNDIR: $!\n";
@tests = TestLib::find_tests($TESTSET, $testname);

if (not @tests) {
    print STDERR "No tests named/matched $testname\n";
    exit(0);
}

foreach my $t (@tests) {
    my @variants = ( undef );
    my $testdir = "$TESTSET/$t";
    if ($t =~ m;(.*)/variants(/.*)?;) {
        $t = $1;
        $testdir = "$TESTSET/$t";
        my $variant = $2//"";
        $variant =~ s;^/;;;
        @variants = ( $variant );
    } elsif ( -e "$testdir/variants") {
        opendir(my $dd, "$testdir/variants");
        push @variants, grep { !/^\./o } sort readdir($dd);
        closedir($dd);
    }

    foreach my $variant (@variants) {
        my $rundir = "$RUNDIR/$t";
        my $testname = $t;
        if ($variant) {
            $testname .= " variant $variant";
            $rundir .= "_$variant";
        }
        my $o = {};
        my $bt = $create_test->($o, $rundir, $testdir, $variant);
        my $res;
        print "Running $testname..." if not $ENV{CI};
        print "===== Running $testname...\n" if $ENV{CI};
        my $ignore_expected = ($bt->testdata ('ignore-expected')//'no') eq 'yes';
        my $timer = $ts->();
        my ($suc, $info);
        eval {
            $bt->setup;
            if ($britney eq 'SETUP-ONLY') {
                $ignore_expected = 1;
                $suc = SUCCESS_EXPECTED;
                $info = '';
            } else {
                ($suc, $info) = $bt->run($britney, $impl);
            }
        };
        if ($@) {
            print "ERROR: $@";
            exit 2 if not $opt{'keep-going'};
            $errors++;
            push @failed, $testname;
            next;
        } elsif ($ignore_expected) {
            $res = ' done';
        } elsif ($suc == SUCCESS_EXPECTED or $suc == FAILURE_EXPECTED
                 or $suc == ERROR_EXPECTED) {
            $res = ' ok';
            if ($suc == FAILURE_EXPECTED) {
                $res = ' expected failure';
                $failed++;
                $expected++;
            } elsif ($suc == ERROR_EXPECTED) {
                $res = ' expected crash';
                $failed++;
                $expected++;
            }
        } else {
            $res = ' FAILED';
            if ($suc == SUCCESS_UNEXPECTED) {
                $res = ' UNEXPECTED SUCCESS';
                $unexpected++;
            } else {
                $failed++;
                push @failed, $testname;
            }
        }
        $res .= " ($info)" unless ($info eq "");
        $count++;
        $res = $res . ( $tf->($timer));
        print "===== Ran $testname..." if $ENV{CI};
        print "$res\n";
    }
}

if ($britney eq 'SETUP-ONLY') {
    print "\nSummary:\n";
    print "Setup directories for $count tests and related variants\n";
} else {
    print "\nSummary:\n";
    print "Ran $count tests\n";
    print "Unexpected successes: $unexpected\n" if $unexpected;
    print "Failed tests: $failed\n";
    print " - of which $expected were expected\n" if $expected;
    print "Errors: $errors\n" if $errors;
    if (scalar @failed) {
        print "Unexpected failures:\n";
        for my $test (@failed) {
            print " - $test\n";
        }
    }
}

exit 1 if $errors or $failed > $expected;
exit 0;

### functions ###

sub _usage {

    print <<EOF ;
Usage: $prog [options] <britney> <testset> <rundir> [<testname>|</regex/>]

Runs the <britney> on a test suite (specified as the <testset>
directory).  The output is stored in the <rundir> directory, which
must not exists.

 -k, --[no-]keep-going
                 With --keep-going, the test continues even if Britney returns
                 a non-zero error code.  Otherwise, it terminates on first
                 error.

If <testname> or </regex/> is given, only the test named <testname> or
tests matching </regex/> (respectively) will be run.  Note that the
regex must start and end with "/".

if <britney> is "SETUP-ONLY", then only the test setup is done but britney
will not be run.  This is useful to prepare the test directories for further
analysis (e.g. to confirm the test data is as expected or to use britney's
hint-tester feature on the test data).

EOF
    exit 0;
}

sub _load_timer {
    my ($ts, $tf);
    eval {
        require Time::HiRes;
        import Time::HiRes qw(gettimeofday tv_interval);

        print "N: Using Time::HiRes to calculate run times\n";
        $ts = sub {
            return [gettimeofday()];
        };
        $tf = sub {
            my ($start) = @_;
            my $diff = tv_interval ($start);
            return sprintf (' (%.3fs)', $diff);
        };
    };

    unless ($ts && $tf) {
        print "N: Fall back to no timing\n";
        $ts = sub { return 0; };
        $tf = sub { return ''; };
    };
    return ($ts, $tf);
}
