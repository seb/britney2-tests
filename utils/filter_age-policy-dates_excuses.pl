#! /usr/bin/perl

use strict;
use Data::Dumper;
use YAML::Syck;

my $excusesfile = shift;
my $datefile = shift;

unless(-e $datefile) {
	die("excusefile datefile?\n");
}

open(DATES,"< $datefile") || die("open $datefile failed\n");

my $ex_data = LoadFile($excusesfile);

my $ex_info = {};

foreach my $s (@{$ex_data->{"sources"}}) {
	#print Dumper $s;
	my $itemn = $s->{"item-name"};
	if (exists($ex_info->{$itemn})) {
		print "duplicate info for $itemn\n";
	}
	$ex_info->{$itemn} = ($s->{"is-candidate"} eq "true");
}

# Filter the datefile to only show the dates for sources that are candidates
# in the excuses.yaml file.
# During tests, other sources will have age 0, so they will not be candidates
# (except if urgented or forced).
while(my $line = <DATES>) {
	my ($pkg, $vers, $stamp) = split(' ',$line);
	if ($ex_info->{$pkg}) {
		print $line;
	}
}

